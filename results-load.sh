#!/bin/sh

DBNAME=$1
TEST=$2
DIR=`pwd`

count=`psql -A -t -c "SELECT COUNT(*) FROM tests where name = '$TEST'" $DBNAME`

if [ "$count" == "0" ]; then

	file=`mktemp`

	grep '] run' $TEST.log  | sed 's/.*] //' | while read line; do

		if [[ $line =~ ^running:.* ]]; then
			patch=`echo $line | sed 's/running: //'`
		fi

		if [[ $line =~ ^run\ .* ]]; then
			x=`echo $line | sed 's/^run \([0-9]*\) \([0-9]*\) threads \([0-9]*\) from \([0-9]*\) to \([0-9]*\) tps \(.*\)$/\1,\2,\3,\4,\5,\6/'`
			echo "$patch,$x" >> $file
		fi

	done

	echo "
	BEGIN;
	INSERT INTO tests (name) VALUES ('$TEST');
	COPY results (patch, run, clients, threads, start, stop, tps) FROM '$file' WITH (FORMAT csv);
	ANALYZE results;
	COPY wait_events (ts, wait_event_type, wait_event, wait_count) FROM '$DIR/$TEST/wait-events.log' WITH (FORMAT csv, DELIMITER '|');
	COPY database_stats (ts, ts_unix, dbsize, datid, datname, numbackends, xact_commit, xact_rollback, blks_read, blks_hit, tup_returned, tup_fetched, tup_inserted, tup_updated, tup_deleted, conflicts, temp_files, temp_bytes, deadlocks, blk_read_time, blk_write_time, stats_reset) FROM '$DIR/$TEST/databases.log' WITH (FORMAT csv, DELIMITER '|');
	COPY xlog_stats (ts, ts_unix, position, bytes) FROM '$DIR/$TEST/xlog.log' WITH (FORMAT csv, DELIMITER '|');
	COMMIT;" | psql "$DBNAME" > /dev/null 2>&1

	if [ $? -ne 0 ]; then
		echo "loading $TEST failed, code", $?
	else
		echo "loaded $TEST"
	fi

else
	echo "skipped $TEST (already loaded)"
fi
