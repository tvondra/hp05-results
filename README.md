clog benchmark results
======================

This repository contains results of a benchmark comparing impact of three
patches, aimed at lowering clog lock contention. The tests were executed on
four different builds:

1) *pg-9.6-master* (master at commit 61f9e7ba)

2) *pg-9.6-group-update* (master at commit 61f9e7ba + patch)

* `group_update_clog_v8.patch` (md5 303b0ff49da07ab4463716c3918423ef)
 
3) *pg-9.6-granular-locking*

* master at commit 61f9e7ba + 2 patches
* `0001-Improve-64bit-atomics-support.patch` (md5 ee266374c605a652205ce4898fcd7d9f)
* `0002-use-granular-locking-v2.patch` (md5 3c04b6761a694b2123915349d877b241)
 
4) *pg-9.6-granular-locking-no-content-lock*

* master at commit 61f9e7ba + 2 patches
* `0001-Improve-64bit-atomics-support.patch` (md5 ee266374c605a652205ce4898fcd7d9f)
* `0002-use-granular-locking-v2.patch` (md5 3c04b6761a694b2123915349d877b241)
* also commented-out `USE_CONTENT_LOCK` macro (src/backend/access/transam/clog.c)


Hardware
--------

The results were collected on a pretty big machine:

* 4 x E7-8890 v3 CPUs (72 cores, 144 with HT)
* 3TB RAM
* P830i RAID controller (4GB write cache)
* 10x 1.92GB 12Gbps SAS SSD (VO1920JEUQQ)
* CentOS 7.2.1511 (with kernel 3.10.0-327.13.1.el7)


Benchmarked cases
-----------------

Each directory contains results for a particular combination of parameters.
The directory name is `${WORKLOAD}-${SCALE}-${WAL}-${SYNC}`, where

* `WORKLOAD` specifies workload type (`pgbench` or `dilip`)
* `SCALE` specifies scale (300 or 3000)
* `WAL` specifies whether tables are WAL-logged (`logged` or `unlogged`)
* `SYNC` specifies `synchronous_commit` value (`sync` or `async`)

For `pgbench` there's additinal parameter `${SKIP}` specifying whether
the benchmark is executed with `-N` to skip updates on the small tables.

To see how the contention changes over time, the benchmark was executed for
multiple client counts: 36, 72, 108, 144, 180, 216, 252, 288. The benchmark
consists of 3 x 5-minute pgbench runs for each client count, interleaved by
a cleanup (`VACUUM ANALYZE` + `CHECKPOINT`).

To see the benchmark details, see

* `run.sh` - a script driving the whole benchmark
* `run-benchmark.sh` - a script testing a particular configuration


Configuration changes
---------------------

The database and kernel configuration was tuned a bit while running the tests.
Initially, the tests were running with `shared_buffers` set to 16GB, and kernel
config left at defaults, including page cache parameters

* `vm.dirty_background_ratio = 10`
* `vm.dirty_ratio = 20`

which on 3TB machine means up to 300GB and 600GB of dirty data. All the tests
with scale 300 (4.5GB) and two tests with scale 3000 were executed with these
parameters:

* dilip-300-logged-async
* dilip-300-logged-sync
* dilip-300-unlogged-async
* dilip-300-unlogged-sync
* pgbench-300-logged-async-noskip
* pgbench-300-logged-async-skip
* pgbench-300-logged-sync-noskip
* pgbench-300-logged-sync-skip
* pgbench-300-unlogged-async-noskip
* pgbench-300-unlogged-async-skip
* pgbench-300-unlogged-sync-noskip
* pgbench-300-unlogged-sync-skip
* dilip-3000-unlogged-sync-high-dirty-bytes
* pgbench-3000-unlogged-sync-skip-high-dirty-bytes

For the small scale (300), the high kernel limits for dirty data are not a big
problem as the whole data set fits into shared buffers and thus there are no
writes except into WAL (which does not use page cache).

For the large data set (scale 3000 is ~45GB of data), the data no longer fits
into shared buffers, the buffers are regularly evicted, resulting in a lot of
dirty data. Due to the excessive kernel limits a lot of dirty data accumulates
in page cache (over 10 GB) and then gets suddenly evicted in short amount of
time, resulting in high tps variability.

After identifying this issue, the kernel limits were modified like this:

* `vm.dirty_background_bytes = 268435456`
* `vm.dirty_ratio = 20`

i.e. the "background" limit was decreased to 256MB (eliminating the sudden
writeouts of large amounts of dirty data by kernel), and the two tests with
scale 3000 were repeated:

* dilip-3000-unlogged-sync
* pgbench-3000-unlogged-sync-skip

After that, the shared buffers were increased to 64GB (which is enough even
for scale 3000), and more tests were performed:

* dilip-3000-unlogged-sync-64
* pgbench-3000-unlogged-sync-skip-64


Analyzing results
-----------------

There are a few (fairly trivial) shell scripts that make analysis easier:

* `results-init.sh $DBNAME` - initialized a database for results
* `results-load.sh $DBNAME $DIR` - loads results from a directory into the db
* `results-analyze.sh $DBNAME` - computes basic reports (prints to stdout)

Furthermore, there are two wrappers:

* `results-rebuild.sh $DBNAME` - initialize the db, load/analyze all results
* `results-update.sh $DBNAME` - load new results, run analysis

