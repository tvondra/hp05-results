#!/bin/sh

DB=$1

./results-init.sh "$DB"

./results-load.sh "$DB" dilip-300-logged-async
./results-load.sh "$DB" dilip-300-logged-sync
./results-load.sh "$DB" dilip-300-unlogged-async
./results-load.sh "$DB" dilip-300-unlogged-sync
./results-load.sh "$DB" dilip-300-unlogged-sync-clog-512
./results-load.sh "$DB" dilip-300-unlogged-async-clog-512

./results-load.sh "$DB" pgbench-300-logged-async-noskip
./results-load.sh "$DB" pgbench-300-logged-async-skip
./results-load.sh "$DB" pgbench-300-logged-sync-noskip
./results-load.sh "$DB" pgbench-300-logged-sync-skip
./results-load.sh "$DB" pgbench-300-unlogged-async-noskip
./results-load.sh "$DB" pgbench-300-unlogged-async-skip
./results-load.sh "$DB" pgbench-300-unlogged-sync-noskip
./results-load.sh "$DB" pgbench-300-unlogged-sync-skip
./results-load.sh "$DB" pgbench-300-unlogged-sync-skip-clog-512
./results-load.sh "$DB" pgbench-300-unlogged-sync-noskip-clog-512

./results-load.sh "$DB" dilip-3000-unlogged-sync
./results-load.sh "$DB" dilip-3000-unlogged-sync-64
./results-load.sh "$DB" dilip-3000-unlogged-sync-high-dirty-bytes
./results-load.sh "$DB" dilip-3000-unlogged-async-64
./results-load.sh "$DB" dilip-3000-logged-sync-64
./results-load.sh "$DB" dilip-3000-logged-async-64
./results-load.sh "$DB" dilip-3000-logged-sync-retest
./results-load.sh "$DB" dilip-3000-logged-sync-retest-512

./results-load.sh "$DB" pgbench-3000-unlogged-sync-skip
./results-load.sh "$DB" pgbench-3000-unlogged-sync-noskip
./results-load.sh "$DB" pgbench-3000-unlogged-sync-skip-64
./results-load.sh "$DB" pgbench-3000-unlogged-sync-skip-high-dirty-bytes
./results-load.sh "$DB" pgbench-3000-unlogged-sync-noskip-64
./results-load.sh "$DB" pgbench-3000-unlogged-async-noskip-64
./results-load.sh "$DB" pgbench-3000-unlogged-async-skip-64
./results-load.sh "$DB" pgbench-3000-logged-sync-noskip-retest
./results-load.sh "$DB" pgbench-3000-logged-sync-noskip-retest-512
./results-load.sh "$DB" pgbench-3000-logged-sync-skip-retest
./results-load.sh "$DB" pgbench-3000-logged-sync-skip-retest-512
./results-load.sh "$DB" pgbench-3000-logged-sync-skip-64
./results-load.sh "$DB" pgbench-3000-logged-sync-noskip-64
./results-load.sh "$DB" pgbench-3000-logged-async-skip-64
./results-load.sh "$DB" pgbench-3000-logged-async-noskip-64

./results-analyze.sh "$DB"
./results-analyze-by-test.sh "$DB"
