#!/bin/bash
# ./run-pgbench.sh $TEST $SCALE $LOGGED $CONFIG $SKIP $SCRIPT

SAVED_PATH=$PATH
DATADIR=/data/tvondra/pgdata
RUNS=3
CLIENTS_WARMUP="72"
CLIENTS="36 72 108 144 180 216 252 288"
DURATION=300
TEST=$1
SCALE=$2
LOGGED=$3
CONFIG=$4
SKIP=$5
SCRIPT=$6
HOMEDIR=`pwd`
DIR=$HOMEDIR/$TEST-running

# skip updates on tellers and branches
N=""
if [ "$SKIP" == "skip" ]; then
	N="-N"
fi

function log {
	echo `date +%s` [`date`] $1
}

# remove results of a partial run (if any) and create new directory
rm -Rf $DIR
mkdir -p $DIR

# collect statistics and wait events in the background
sh collect-stats.sh $DIR &
sh collect-wait-events.sh $DIR &

cd $DIR

log "TEST $TEST START"

for d in pg-9.6-master pg-9.6-group-update pg-9.6-granular-locking pg-9.6-granular-locking-no-content-lock; do

	log "running: $d"

	# create directory for the results
	mkdir $DIR/$d
	cd $DIR/$d

	export PATH=/home/tvondra/clog-builds/$d/bin:$SAVED_PATH

	# make sure there's nothing running (and be strict about it)
	killall -9 postgres > /dev/null 2>&1

	# give the system a while to handle the kill
	sleep 15

	rm -Rf $DATADIR

	which pg_config > which.log 2>&1

	pg_config  > config.log 2>&1

	log "initialize cluster"

	pg_ctl -D $DATADIR init > init.log 2>&1

	cp $HOMEDIR/$CONFIG $DATADIR/postgresql.conf

	pg_ctl -D $DATADIR -l pg.log -w start > start.log 2>&1

	psql -c "select * from pg_settings" postgres > settings.log 2>&1

	createdb pgbench > createdb.log 2>&1

	log "initialize database"

	if [ "$LOGGED" == "unlogged" ]; then
		pgbench -i -s $SCALE --unlogged-tables pgbench > init-data.log 2>&1
	else
		pgbench -i -s $SCALE pgbench > init-data.log 2>&1
	fi

	log "warmup"

	if [ "$SCRIPT" == "" ]; then
		pgbench -c $CLIENTS_WARMUP -j 8 -M prepared $N -T $DURATION pgbench > warmup.log 2>&1
	else
		pgbench -c $CLIENTS_WARMUP -j 8 -M prepared -f $HOMEDIR/$SCRIPT -T $DURATION pgbench > warmup.log 2>&1
	fi

	log "benchmark"

	for r in `seq 1 $RUNS`; do

		for c in $CLIENTS; do

			psql -c "vacuum analyze" pgbench > /dev/null 2>&1
			psql -c "checkpoint" postgres > /dev/null 2>&1

                        t=$((c/4))

                        if [ $t -eq 0 ]; then
                               	t=1
                       	fi

			ts=`date +%s`

			if [ "$SCRIPT" == "" ]; then
				pgbench -c $c -j $t -M prepared $N -T $DURATION pgbench > clients-$c-$r.log 2>&1;
			else
				pgbench -c $c -j $t -M prepared -f $HOMEDIR/$SCRIPT -T $DURATION pgbench > clients-$c-$r.log 2>&1;
			fi

			ts2=`date +%s`

			tps=`grep excluding clients-$c-$r.log | awk '{print $3}'`

			log "run $r $clients $c threads $t from $ts to $ts2 tps $tps"

		done;

	done;

	log "stop cluster"

	# wait for 5 minutes for the database to stop
	pg_ctl -D $DATADIR -w -t 300 stop > /dev/null 2>&1

done

cd $HOMEDIR

# kill the background jobs collecting stats
for p in `jobs -p`; do
	kill $p
done

# rename the result directory to make it clear the benchmark completed
mv $DIR "$HOMEDIR/$TEST"

log "TEST $TEST DONE"
