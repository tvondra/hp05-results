#!/bin/sh

DBNAME=$1

TESTS=`psql -A -t -c "SELECT name FROM tests" results`

for TEST in $TESTS; do

	mkdir -p reports/by-test/$TEST;

	# average tps for different cases
	echo "summary for $TEST" > reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "SELECT patch, clients, median_tps, avg_tps, min_tps, max_tps, delta_tps, tps FROM summary_stats WHERE test = '$TEST' ORDER BY 1, 2" >> reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "COPY(SELECT patch, clients, median_tps, avg_tps, min_tps, max_tps, delta_tps, tps FROM summary_stats WHERE test = '$TEST' ORDER BY 1, 2) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/average-tps.csv 2>&1

	echo "tps average per patch" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		clients,
		avg_tps AS master,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') * 100 / avg_tps AS granular_locking_pct,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') * 100 / avg_tps AS no_content_lock_pct,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') * 100 / avg_tps AS group_update_pct
	FROM tps_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		clients,
		avg_tps AS master,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') * 100 / avg_tps AS granular_locking_pct,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') * 100 / avg_tps AS no_content_lock_pct,
		(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') * 100 / avg_tps AS group_update_pct
	FROM tps_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/average-tps-by-patch.csv 2>&1

	echo "tps median per patch" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		clients,
		p50_tps AS master,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') * 100 / p50_tps AS granular_locking_pct,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') * 100 / p50_tps AS no_content_lock_pct,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') * 100 / p50_tps AS group_update_pct
	FROM tps_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		clients,
		p50_tps AS master,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') * 100 / p50_tps AS granular_locking_pct,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') * 100 / p50_tps AS no_content_lock_pct,
		(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') * 100 / p50_tps AS group_update_pct
	FROM tps_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/median-tps-by-patch.csv 2>&1

	echo "tps stddev per patch" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		clients,
		stddev_tps AS master,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') * 100 / stddev_tps AS granular_locking_pct,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') * 100 / stddev_tps  AS no_content_lock_pct,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') * 100 / stddev_tps  AS group_update_pct
	FROM tps_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		clients,
		stddev_tps AS master,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') * 100 / stddev_tps AS granular_locking_pct,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') * 100 / stddev_tps  AS no_content_lock_pct,
		(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') * 100 / stddev_tps  AS group_update_pct
	FROM tps_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/stddev-tps-by-patch.csv 2>&1

	echo "wait events for $TEST (patch comparison, count)" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		clients,
		wait_event_type,
		wait_event,
		wait_count AS master,
		(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-group-update') AS group_update
	FROM wait_events_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1, 4 DESC, 2, 3" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		clients,
		wait_event_type,
		wait_event,
		wait_count AS master,
		(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-group-update') AS group_update
	FROM wait_events_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1, 4 DESC, 2, 3) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/wait-events-count.csv 2>&1

	echo "wait events for $TEST (patch comparison, percentage)" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		clients,
		wait_event_type,
		wait_event,
		wait_pct AS master,
		(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-group-update') AS group_update
	FROM wait_events_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1, 4 DESC, 2, 3" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		clients,
		wait_event_type,
		wait_event,
		wait_pct AS master,
		(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking') AS granular_locking,
		(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
		(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-group-update') AS group_update
	FROM wait_events_stats s
	WHERE patch = 'pg-9.6-master' AND test = '$TEST'
	ORDER BY 1, 4 DESC, 2, 3) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/wait-events-pct.csv 2>&1

	# wait events (runs combined)
	echo "wait events for $TEST (runs combined)" >> reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "SELECT patch, clients, wait_event_type, wait_event, wait_count, wait_pct FROM wait_events_stats WHERE test = '$TEST' ORDER BY 2, 1, 5 DESC, 3, 4" >> reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "COPY (SELECT patch, clients, wait_event_type, wait_event, wait_count, wait_pct FROM wait_events_stats WHERE test = '$TEST' ORDER BY 2, 1, 5 DESC, 3, 4) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/wait-events-combined.csv 2>&1

	# tps statistics
	echo "tps statistics for $TEST" >> reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "SELECT patch, clients, run, avg_tps, stddev_tps, p25_tps, p50_tps, p75_tps, p95_tps FROM tps_stats_per_run WHERE test = '$TEST' ORDER BY 2, 1, 3" >> reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "COPY (SELECT patch, clients, run, avg_tps, stddev_tps, p25_tps, p50_tps, p75_tps, p95_tps FROM tps_stats_per_run WHERE test = '$TEST' ORDER BY 2, 1, 3) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/tps-stats.csv 2>&1

	# xlog statistics
	echo "xlog statistics" >> reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "SELECT patch, clients, run, tps, wal_bytes, wal_mbytes FROM xlog_stats_per_run WHERE test = '$TEST' ORDER BY 2, 1, 3" >> reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "COPY (SELECT patch, clients, run, tps, wal_bytes, wal_mbytes FROM xlog_stats_per_run WHERE test = '$TEST' ORDER BY 2, 1, 3) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/xlog-stats.csv 2>&1

	echo "wait event counts by patch" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		patch,
		clients,
		wait_event_type,
		wait_event,
		wait_count,
		(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 1) AS wait_count_1,
		(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 2) AS wait_count_2,
		(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 3) AS wait_count_3
	FROM wait_events_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2, 5 DESC, 3, 4" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		patch,
		clients,
		wait_event_type,
		wait_event,
		wait_count,
		(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 1) AS wait_count_1,
		(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 2) AS wait_count_2,
		(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 3) AS wait_count_3
	FROM wait_events_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2, 5 DESC, 3, 4) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/wait-events-counts-per-run.csv 2>&1

	echo "wait event percent by patch" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		patch,
		clients,
		wait_event_type,
		wait_event,
		wait_pct,
		(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 1) AS wait_pct_1,
		(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 2) AS wait_pct_2,
		(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 3) AS wait_pct_3
	FROM wait_events_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2, 5 DESC, 3, 4" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		patch,
		clients,
		wait_event_type,
		wait_event,
		wait_pct,
		(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 1) AS wait_pct_1,
		(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 2) AS wait_pct_2,
		(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 3) AS wait_pct_3
	FROM wait_events_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2, 5 DESC, 3, 4) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/wait-events-pct-per-run.csv 2>&1

	echo "average tps compared over runs / per patch" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		patch,
		clients,
		avg_tps,
		(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS avg_tps_1,
		(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS avg_tps_2,
		(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS avg_tps_3
	FROM tps_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		patch,
		clients,
		avg_tps,
		(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS avg_tps_1,
		(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS avg_tps_2,
		(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS avg_tps_3
	FROM tps_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/avg-tps-per-run.csv 2>&1

	echo "tps stddev compared over runs / per patch" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		patch,
		clients,
		stddev_tps,
		(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS stddev_tps_1,
		(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS stddev_tps_2,
		(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS stddev_tps_3
	FROM tps_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		patch,
		clients,
		stddev_tps,
		(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS stddev_tps_1,
		(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS stddev_tps_2,
		(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS stddev_tps_3
	FROM tps_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/stddev-tps-per-run.csv 2>&1

	echo "tps median compared over runs / per patch" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "SELECT
		patch,
		clients,
		p50_tps,
		(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS p50_tps_1,
		(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS p50_tps_2,
		(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS p50_tps_3
	FROM tps_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2" >> reports/by-test/$TEST.txt 2>&1

	psql $DBNAME -c "COPY (SELECT
		patch,
		clients,
		p50_tps,
		(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS p50_tps_1,
		(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS p50_tps_2,
		(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS p50_tps_3
	FROM tps_stats s
	WHERE test = '$TEST'
	ORDER BY 1, 2) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/median-tps-per-run.csv 2>&1

	# wait events (per run)
	echo "wait events for $TEST (per run)" >> reports/by-test/$TEST.txt 2>&1
	psql $DBNAME -c "SELECT patch, clients, run, wait_event_type, wait_event, wait_count, wait_pct FROM wait_events_stats_per_run WHERE test = '$TEST' ORDER BY 2, 1, 3, 6 DESC, 4, 5" >> reports/by-test/$TEST.txt 2> /dev/null
	psql $DBNAME -c "COPY (SELECT patch, clients, run, wait_event_type, wait_event, wait_count, wait_pct FROM wait_events_stats_per_run WHERE test = '$TEST' ORDER BY 2, 1, 3, 6 DESC, 4, 5) TO stdout WITH (FORMAT csv, HEADER true)" > reports/by-test/$TEST/wait-events-per-run.csv 2> /dev/null

done
