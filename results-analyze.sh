#!/bin/sh

DBNAME=$1

# collect stats first
psql $DBNAME -c "ANALYZE" > /dev/null 2>&1

psql $DBNAME -c "DROP TABLE IF EXISTS summary_stats" > /dev/null 2>&1
psql $DBNAME -c "DROP TABLE IF EXISTS wait_events_stats_per_run" > /dev/null 2>&1
psql $DBNAME -c "DROP TABLE IF EXISTS wait_events_stats" > /dev/null 2>&1
psql $DBNAME -c "DROP TABLE IF EXISTS tps_stats_per_run" > /dev/null 2>&1
psql $DBNAME -c "DROP TABLE IF EXISTS tps_stats" > /dev/null 2>&1
psql $DBNAME -c "DROP TABLE IF EXISTS xlog_stats_per_run" > /dev/null 2>&1

# average tps for different cases
psql $DBNAME -c "CREATE TABLE summary_stats AS
SELECT
	r.test_id,
	(SELECT name FROM tests WHERE id = r.test_id) AS test,
	r.patch,
	r.clients,
	percentile_cont(0.5) WITHIN GROUP (ORDER BY r.tps) AS median_tps,
	avg(r.tps)::int AS avg_tps,
	min(r.tps)::int AS min_tps,
	max(r.tps)::int AS max_tps,
	round((100 * (max(r.tps) - min(r.tps)) / avg(r.tps))::numeric, 1) AS delta_tps,
	(SELECT array_agg(x) FROM (SELECT tps::int AS x FROM results WHERE test_id = r.test_id AND patch = r.patch AND clients = r.clients ORDER BY run) foo) AS tps
FROM results r
GROUP BY 1, 2, 3, 4" > /dev/null 2>&1

psql $DBNAME -c "SELECT test, patch, clients, median_tps, avg_tps, min_tps, max_tps, delta_tps, tps FROM summary_stats ORDER BY 1, 2, 3" > reports/summary.txt 2> /dev/null
psql $DBNAME -c "COPY (SELECT test, patch, clients, median_tps, avg_tps, min_tps, max_tps, delta_tps, tps FROM summary_stats ORDER BY 1, 2, 3) TO stdout WITH (FORMAT csv, HEADER true)" > reports/summary.csv 2> /dev/null

# wait events (per run)
psql $DBNAME -c 'CREATE TABLE wait_events_stats_per_run AS
SELECT
	t.name AS test,
	patch,
	clients,
	run,
	wait_event_type,
	wait_event,
	wait_count,
	round(100.0 * wait_count / SUM(wait_count) OVER (PARTITION BY test_id, patch, clients, run), 2) AS wait_pct
FROM
(
	SELECT
		r.test_id,
		r.patch,
		r.clients,
		r.run,
		e.wait_event_type,
		e.wait_event,
		sum(e.wait_count) AS wait_count
	FROM results r JOIN wait_events e ON (e.run_id = r.id)
	GROUP BY 1, 2, 3, 4, 5, 6
) foo JOIN tests t ON (test_id = t.id)' > /dev/null 2>&1

psql $DBNAME -c "CREATE INDEX ON wait_events_stats_per_run(test, patch, clients, run)" > /dev/null 2>&1

psql $DBNAME -c "SELECT * FROM wait_events_stats_per_run ORDER BY 1, 3, 2, 4, 7 DESC, 5, 6" > reports/wait-events-per-run.txt 2> /dev/null
psql $DBNAME -c "COPY (SELECT * FROM wait_events_stats_per_run ORDER BY 1, 3, 2, 4, 7 DESC, 5, 6) TO stdout WITH (FORMAT csv, HEADER true)" > reports/wait-events-per-run.csv 2> /dev/null

# wait events (runs combined)
psql $DBNAME -c 'CREATE TABLE wait_events_stats AS
SELECT
	t.name AS test,
	patch,
	clients,
	wait_event_type,
	wait_event,
	wait_count,
	round(100.0 * wait_count / SUM(wait_count) OVER (PARTITION BY test_id, patch, clients), 2) AS wait_pct
FROM
(
	SELECT
		r.test_id,
		r.patch,
		r.clients,
		e.wait_event_type,
		e.wait_event,
		sum(e.wait_count) AS wait_count
	FROM results r JOIN wait_events e ON (e.run_id = r.id)
	GROUP BY 1, 2, 3, 4, 5
) foo JOIN tests t ON (test_id = t.id)' > /dev/null 2>&1

psql $DBNAME -c "CREATE INDEX ON wait_events_stats(test, patch, clients)" > /dev/null 2>&1

psql $DBNAME -c "SELECT * FROM wait_events_stats ORDER BY 1, 3, 2, 6 DESC, 4, 5" > reports/wait-events.txt 2> /dev/null
psql $DBNAME -c "COPY (SELECT * FROM wait_events_stats ORDER BY 1, 3, 2, 6 DESC, 4, 5) TO stdout WITH (FORMAT csv, HEADER true)" > reports/wait-events.csv 2> /dev/null

# tps statistics
psql $DBNAME -c "CREATE TABLE tps_stats_per_run AS
SELECT
	t.name AS test,
	patch,
	clients,
	run,
	AVG(tps)::int AS avg_tps,
	STDDEV(xtps)::int AS stddev_tps,
	(percentile_cont(0.25) WITHIN GROUP (ORDER BY xtps))::int AS p25_tps,
	(percentile_cont(0.50) WITHIN GROUP (ORDER BY xtps))::int AS p50_tps,
	(percentile_cont(0.75) WITHIN GROUP (ORDER BY xtps))::int AS p75_tps,
	(percentile_cont(0.95) WITHIN GROUP (ORDER BY xtps))::int AS p95_tps
FROM
(
	SELECT
		r.test_id,
		r.patch,
		r.clients,
		r.run,
		r.tps,
		s.ts_unix,
		(s.xact_commit - LAG(s.xact_commit, 1) OVER (PARTITION BY r.test_id, r.patch, r.clients, r.run ORDER BY s.ts_unix)) / (s.ts_unix - LAG(s.ts_unix, 1) OVER (PARTITION BY r.test_id, r.patch, r.clients, r.run ORDER BY s.ts_unix)) AS xtps
	FROM results r JOIN database_stats s ON (s.run_id = r.id)
	WHERE datname = 'pgbench'
) stats JOIN tests t ON (test_id = t.id)
WHERE xtps IS NOT NULL
GROUP BY 1, 3, 2, 4" > /dev/null 2>&1

psql $DBNAME -c "CREATE INDEX ON tps_stats_per_run(test, patch, clients, run)" > /dev/null 2>&1

psql $DBNAME -c "SELECT * FROM tps_stats_per_run ORDER BY 1, 3, 2, 4" > reports/tps-per-run.txt 2> /dev/null
psql $DBNAME -c "COPY (SELECT * FROM tps_stats_per_run ORDER BY 1, 3, 2, 4) TO stdout WITH (FORMAT csv, HEADER true)" > reports/tps-per-run.csv 2> /dev/null

# tps statistics
psql $DBNAME -c "CREATE TABLE tps_stats AS
SELECT
	t.name AS test,
	patch,
	clients,
	AVG(tps)::int AS avg_tps,
	STDDEV(xtps)::int AS stddev_tps,
	(percentile_cont(0.25) WITHIN GROUP (ORDER BY xtps))::int AS p25_tps,
	(percentile_cont(0.50) WITHIN GROUP (ORDER BY xtps))::int AS p50_tps,
	(percentile_cont(0.75) WITHIN GROUP (ORDER BY xtps))::int AS p75_tps,
	(percentile_cont(0.95) WITHIN GROUP (ORDER BY xtps))::int AS p95_tps
FROM
(
	SELECT
		r.test_id,
		r.patch,
		r.clients,
		r.tps,
		s.ts_unix,
		(s.xact_commit - LAG(s.xact_commit, 1) OVER (PARTITION BY r.test_id, r.patch, r.clients ORDER BY s.ts_unix)) / (s.ts_unix - LAG(s.ts_unix, 1) OVER (PARTITION BY r.test_id, r.patch, r.clients ORDER BY s.ts_unix)) AS xtps
	FROM results r JOIN database_stats s ON (s.run_id = r.id)
	WHERE datname = 'pgbench'
) stats JOIN tests t ON (test_id = t.id)
WHERE xtps IS NOT NULL
GROUP BY 1, 2, 3" > /dev/null 2>&1

psql $DBNAME -c "CREATE INDEX ON tps_stats(test, patch, clients)" > /dev/null 2>&1

psql $DBNAME -c "SELECT * FROM tps_stats_per_run ORDER BY 1, 3, 2, 4" > reports/tps.txt 2> /dev/null
psql $DBNAME -c "COPY (SELECT * FROM tps_stats_per_run ORDER BY 1, 3, 2, 4) TO stdout WITH (FORMAT csv, HEADER true)" > reports/tps.csv 2> /dev/null

# xlog statistics
psql $DBNAME -c "CREATE TABLE xlog_stats_per_run AS
SELECT
	t.name AS test,
	patch,
	clients,
	run,
	tps::int as tps,
	wal_bytes,
	(wal_bytes / 1024 / 1024) AS wal_mbytes
FROM
(
	SELECT
		r.test_id,
		r.patch,
		r.clients,
		r.run,
		r.tps,
		MAX(bytes) - MIN(bytes) AS wal_bytes
	FROM results r JOIN xlog_stats s ON (s.run_id = r.id)
	GROUP BY 1, 2, 3, 4, 5
	ORDER BY 1, 2, 3, 4, 5
) stats JOIN tests t ON (test_id = t.id)" > /dev/null 2>&1

psql $DBNAME -c "CREATE INDEX ON xlog_stats_per_run(test, patch, clients, run)" > /dev/null 2>&1

psql $DBNAME -c "SELECT * FROM xlog_stats_per_run ORDER BY 1, 3, 2, 4" > reports/xlog-stats-per-run.txt 2> /dev/null
psql $DBNAME -c "COPY (SELECT * FROM xlog_stats_per_run ORDER BY 1, 3, 2, 4) TO stdout WITH (FORMAT csv, HEADER true)" > reports/xlog-stats-per-run.csv 2> /dev/null

psql $DBNAME -c "ANALYZE" > /dev/null 2>&1

psql $DBNAME -c "SELECT
	test,
	patch,
	clients,
	wait_event_type,
	wait_event,
	wait_count,
	(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 1) AS wait_count_1,
	(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 2) AS wait_count_2,
	(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 3) AS wait_count_3
FROM wait_events_stats s
ORDER BY 1, 2, 3, 6 DESC, 4, 5" > reports/wait-events-count-runs.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	patch,
	clients,
	wait_event_type,
	wait_event,
	wait_count,
	(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 1) AS wait_count_1,
	(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 2) AS wait_count_2,
	(SELECT wait_count FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 3) AS wait_count_3
FROM wait_events_stats s
ORDER BY 1, 2, 3, 6 DESC, 4, 5) TO stdout WITH (FORMAT csv, HEADER true)" > reports/wait-events-count-runs.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	clients,
	wait_event_type,
	wait_event,
	wait_count AS master,
	(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-group-update') AS group_update
FROM wait_events_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2, 5 DESC, 3, 4" > reports/wait-events-count-patches.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	clients,
	wait_event_type,
	wait_event,
	wait_count AS master,
	(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT wait_count FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-group-update') AS group_update
FROM wait_events_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2, 5 DESC, 3, 4) TO stdout WITH (FORMAT csv, HEADER true)" > reports/wait-events-count-patches.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	patch,
	clients,
	wait_event_type,
	wait_event,
	wait_pct,
	(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 1) AS wait_pct_1,
	(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 2) AS wait_pct_2,
	(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 3) AS wait_pct_3
FROM wait_events_stats s
ORDER BY 1, 2, 3, 6 DESC, 4, 5" > reports/wait-events-pct-runs.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	patch,
	clients,
	wait_event_type,
	wait_event,
	wait_pct,
	(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 1) AS wait_pct_1,
	(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 2) AS wait_pct_2,
	(SELECT wait_pct FROM wait_events_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND run = 3) AS wait_pct_3
FROM wait_events_stats s
ORDER BY 1, 2, 3, 6 DESC, 4, 5) TO stdout WITH (FORMAT csv, HEADER true)" > reports/wait-events-pct-runs.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	clients,
	wait_event_type,
	wait_event,
	wait_pct AS master,
	(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-group-update') AS group_update
FROM wait_events_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2, 5 DESC, 3, 4" > reports/wait-events-pct-patches.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	clients,
	wait_event_type,
	wait_event,
	wait_pct AS master,
	(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT wait_pct FROM wait_events_stats WHERE test = s.test AND clients = s.clients AND wait_event_type IS NOT DISTINCT FROM s.wait_event_type AND wait_event IS NOT DISTINCT FROM s.wait_event AND patch = 'pg-9.6-group-update') AS group_update
FROM wait_events_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2, 5 DESC, 3, 4) TO stdout WITH (FORMAT csv, HEADER true)" > reports/wait-events-pct-patches.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	patch,
	clients,
	avg_tps,
	(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS avg_tps_1,
	(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS avg_tps_2,
	(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS avg_tps_3
FROM tps_stats s
ORDER BY 1, 2, 3" > reports/tps-avg-runs.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	patch,
	clients,
	avg_tps,
	(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS avg_tps_1,
	(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS avg_tps_2,
	(SELECT avg_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS avg_tps_3
FROM tps_stats s
ORDER BY 1, 2, 3) TO stdout WITH (FORMAT csv, HEADER true)" > reports/tps-avg-runs.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	clients,
	avg_tps AS master,
	(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update
FROM tps_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2" > reports/tps-avg-patches.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	clients,
	avg_tps AS master,
	(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT avg_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update
FROM tps_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2) TO stdout WITH (FORMAT csv, HEADER true)" > reports/tps-avg-patches.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	patch,
	clients,
	stddev_tps,
	(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS stddev_tps_1,
	(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS stddev_tps_2,
	(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS stddev_tps_3
FROM tps_stats s
ORDER BY 1, 2, 3" > reports/tps-stddev-runs.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	patch,
	clients,
	stddev_tps,
	(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS stddev_tps_1,
	(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS stddev_tps_2,
	(SELECT stddev_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS stddev_tps_3
FROM tps_stats s
ORDER BY 1, 2, 3) TO stdout WITH (FORMAT csv, HEADER true)" > reports/tps-stddev-runs.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	clients,
	stddev_tps AS master,
	(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update
FROM tps_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2" > reports/tps-stddev-patches.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	clients,
	stddev_tps AS master,
	(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT stddev_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update
FROM tps_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2) TO stdout WITH (FORMAT csv, HEADER true)" > reports/tps-stddev-patches.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	patch,
	clients,
	p50_tps,
	(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS p50_tps_1,
	(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS p50_tps_2,
	(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS p50_tps_3
FROM tps_stats s
ORDER BY 1, 2, 3" > reports/tps-median-runs.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	patch,
	clients,
	p50_tps,
	(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 1) AS p50_tps_1,
	(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 2) AS p50_tps_2,
	(SELECT p50_tps FROM tps_stats_per_run WHERE test = s.test AND patch = s.patch AND clients = s.clients AND run = 3) AS p50_tps_3
FROM tps_stats s
ORDER BY 1, 2, 3) TO stdout WITH (FORMAT csv, HEADER true)" > reports/tps-median-runs.csv 2>&1;

psql $DBNAME -c "SELECT
	test,
	clients,
	p50_tps AS master,
	(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update
FROM tps_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2" > reports/tps-median-patches.txt 2>&1;

psql $DBNAME -c "COPY (SELECT
	test,
	clients,
	p50_tps AS master,
	(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking') AS granular_locking,
	(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-granular-locking-no-content-lock') AS no_content_lock,
	(SELECT p50_tps FROM tps_stats WHERE test = s.test AND clients = s.clients AND patch = 'pg-9.6-group-update') AS group_update
FROM tps_stats s
WHERE patch = 'pg-9.6-master'
ORDER BY 1, 2) TO stdout WITH (FORMAT csv, HEADER true)" > reports/tps-median-patches.csv 2>&1;
