#!/bin/sh

DBNAME=$1

dropdb --if-exists "$DBNAME"
createdb "$DBNAME"

psql "$DBNAME" -c "create table tests (id SERIAL PRIMARY KEY, name TEXT)"

psql "$DBNAME" -c 'create or replace function update_run_id() returns trigger as $$
declare
	v_run_id int;
begin

	SELECT id INTO v_run_id FROM results WHERE test_id = NEW.test_id AND NEW.ts BETWEEN start AND stop;
	NEW.run_id := v_run_id;

	-- ignore samples not falling into one of the runs
	IF v_run_id IS NULL THEN
		RETURN NULL;
	END IF;

	RETURN NEW;
end
$$ language plpgsql';

psql "$DBNAME" -c 'create or replace function update_run_id2() returns trigger as $$
declare
	v_run_id int;
begin

	SELECT id INTO v_run_id FROM results WHERE test_id = NEW.test_id AND NEW.ts_unix BETWEEN start AND stop;
	NEW.run_id := v_run_id;

	-- ignore samples not falling into one of the runs
	IF v_run_id IS NULL THEN
		RETURN NULL;
	END IF;

	RETURN NEW;
end
$$ language plpgsql';

psql "$DBNAME" -c "create table results (
	id SERIAL PRIMARY KEY,
	test_id INT NOT NULL DEFAULT currval('tests_id_seq') REFERENCES tests(id),
	patch text,
	run int,
	clients int,
	threads int,
	start float,
	stop float,
	tps float)"

psql "$DBNAME" -c "create table wait_events (
	test_id INT NOT NULL DEFAULT currval('tests_id_seq') REFERENCES tests(id),
	run_id  INT NOT NULL REFERENCES results(id),
	ts float,
	wait_event_type text,
	wait_event text,
	wait_count int)"

psql "$DBNAME" -c "create trigger lookup_run before insert on wait_events for each row execute procedure update_run_id()";

psql "$DBNAME" -c "create table database_stats (
	test_id INT NOT NULL DEFAULT currval('tests_id_seq') REFERENCES tests(id),
	run_id  INT NOT NULL REFERENCES results(id),
	ts             timestamp with time zone,
	ts_unix        float,
	dbsize         bigint,
	datid          oid,
	datname        name,
	numbackends    integer,
	xact_commit    bigint,
	xact_rollback  bigint,
	blks_read      bigint,
	blks_hit       bigint,
	tup_returned   bigint,
	tup_fetched    bigint,
	tup_inserted   bigint,
	tup_updated    bigint,
	tup_deleted    bigint,
	conflicts      bigint,
	temp_files     bigint,
	temp_bytes     bigint,
	deadlocks      bigint,
	blk_read_time  double precision,
	blk_write_time double precision,
	stats_reset    timestamp with time zone
)"

psql "$DBNAME" -c "create trigger lookup_run before insert on database_stats for each row execute procedure update_run_id2()";

psql "$DBNAME" -c "create table xlog_stats (
	test_id INT NOT NULL DEFAULT currval('tests_id_seq') REFERENCES tests(id),
	run_id  INT NOT NULL REFERENCES results(id),
	ts             timestamp with time zone,
	ts_unix        float,
	position       pg_lsn,
	bytes          bigint
)"

psql "$DBNAME" -c "create trigger lookup_run before insert on xlog_stats for each row execute procedure update_run_id2()";

psql "$DBNAME" -c "create unique index on results (test_id, patch, run, clients)"
psql "$DBNAME" -c "create unique index on results (test_id, start, stop)"

psql "$DBNAME" -c "create unique index on results (start, stop, test_id)"
psql "$DBNAME" -c "create index on wait_events (ts)"

psql "$DBNAME" -c "create index on database_stats (ts_unix)"
psql "$DBNAME" -c "create index on xlog_stats (ts_unix)"
